.. CosmoSIS notes documentation master file, created by
   sphinx-quickstart on Wed Dec 14 21:28:38 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CosmoSIS notes's documentation!
==========================================

Contents:

.. toctree::
   :maxdepth: 4

   INFO.rst
   cosmosis


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

