
#######
General
#######

Suspected bugs
==============

        * 01/08/2016
          sigz = 0 causes projected spectra (\*_cl) to be nan
          Need to set to very small value
          Updates:
                - 08/03/2017
                  Bug report created:

        * 10/2016 or so
          sigz very small value created problem for zmin>0 (n(z=0) was not zero).
          Added case for sigz=0.

        * 05/12/2016
          sigz=0 case causes all n_i(z) to be equal for tomography.
          Fixed this bug! Needed to replace prob(zph|ztr) = 1 -> delta(zph-ztr)

        * 07/12/2016
          Exchanging columns and rows in z_prob_matrix: nz coincides with nicaea,
          if for the latter I choose the same bins as CosmoSIS!

        * 09/12/2016
          dz=0.0003 runs fine
          dz=0.001: not symmetrical covariance

        * nan's in projected spectra (lensing) if x_analyse.ini:ell_min >
          x_simulate.py:ell_min  (within simulate it has to be consistent,
          if not an error is already produced)
          Updates:
                - 08/03/2017
                  Bug report created:
                  https://bitbucket.org/joezuntz/cosmosis/issues/225/projected-power-spectr-interpolation

        * wrong covariance, due to incorrect lookup table
          27/04/2017 bug report created:
          https://bitbucket.org/joezuntz/cosmosis/issues/239/wrong-covariance-lookup-splines-for

Bug reporting
=============

https://bitbucket.org/joezuntz/cosmosis/issues?status=new&status=open

Debugging
=========

For breakpoint use absolute path to py file.

Important things to remember
============================

        * Use A_s: Do not use sigma8_rescale.py module (neither in simulate nor
          in analyse!), do not set sigma8_input in value file

        * When changing parameters (e.g. A_s vs. sigma8), best to re-run
          simulation file

        * 5/9/2016: Created fork of ./cosmosis:
          https://bitbucket.org/martin_kilbinger/cosmosis-euclid-ist-ext

        * load_nz: z-bins should start at 0, otherwise they will be padded
          and might not be equidistant any more.

        * options, block, get all variables:
          options.keys()
          options.sections()


Documentation
=============

        * 2pt_like
          https://github.com/joezuntz/2point/
        * writing new module
          https://bitbucket.org/joezuntz/cosmosis/wiki/creating_modules
        * fork
          https://bitbucket.org/joezuntz/cosmosis/wiki/Where%20to%20put%20CosmoSIS-compatible%20code

Todo
====

        * load_nz, smail:
          some things only work for equidistany z-bins

        * photometry_smail:
          gaussian convolution strange with zmin, zmax

######
Euclid
######

nan             with lmin bug, NaN's

Simulations
===========
        1. euclid_simulation_wIA.fits
           with IA
        2. euclid_simulation_no_addIA.fits
           without add_intrinsic
        3. euclid_simulation_addIA-F.fits
           add_intrinsic flags F
           shear, galaxy_shear, galaxy as 2.
        4. euclid_simulation_addIA-F_2Cl_IA-F.fits
           add_intrinsic flags F, pk_to_cl \*-intrinsic spectra F
           shear, galaxy_shear, galaxy as 2.

Analysis
========

        * pk_to_cl shear-shear = F no difference if shear_cl not used

        * zmin_0.1_spectra
                Setting zmin in shear_shear, interface, etc., no longer used
        * zmin_0.1_smail
                Setting zmin in smail
        * zmin_0.1_load_nz
                From smail simulation output, create nz histogram to load.

Checks
======

A_s - b degeneracy
------------------

        * With 5 parameters, marginalized 2D constraints in same direction, not anti-correlated.
          True for ln 10^10 A_s and A_s.
        * 2 parameters ok, so it's effect of marginalization over the other parameters, or numerical
          issue
        * Bias and ln (10^10 A_s) derivatives ok for GC, WL, WL+GC when varying 2 or 5 parameters
          (euclid_forecast_WL_2DGC_1bin/linear/A_s-b)
        * Checking: GC alone; A_s instead of ln A_s, ok.
        * Bias in code checked, seems ok.

Derivatives, normalisation
--------------------------

        * Xc_1/unity_priors: normalised and un-normalised derivatives are the same
          needed to set step_size=0.01, otherwise omegabh2 was too close to lower bound
        * Xc_1/step_size_0.01: normalised derivs are same as above, small differences
        * Xc_1/two-point: two-point stencil test

        * Beg 04/2017: .ini files copied to bitbucket repo, setting links
        * 10/04/2017: ngal = 6.5 -> 30

Xc_1
----

Subdirectories
^^^^^^^^^^^^^^
        * lmax_100[_ngal6.5]        Run with lmax=100. Still depends a lot on shot noise.

        * WL, GC, WLGC, ...
          galaxy_shear only xcorrel: still depends on shot noise, since <gn gn> = <gg><nn> + ...,
          so contributions from auto-correlations



Change variables
^^^^^^^^^^^^^^^^

+-------------+-------+---------------------+-------------+---------------+-------------------+
| Variable    | value | margin. err. change | err. change | Fisher change | inv Fisher change |
+=============+=======+=====================+=============+===============+===================+
| dz          | 0.001 | 2-5%                | no change   | sub %         | 15%               |
+-------------+-------+---------------------+-------------+---------------+-------------------+
| nell(comm)  | 25    | 10%                 | 1%          | 1%            | 25%               |
+-------------+-------+---------------------+-------------+---------------+-------------------+
| nell(both)  | 25    | 4%                  | 4%          | 8%            | 8%                |
+-------------+-------+---------------------+-------------+---------------+-------------------+
| kmax        | 250   | no                  | no          | < double      | < float           |
+-------------+-------+---------------------+-------------+---------------+-------------------+
| ellmax(pk2) | 5000  | 3%                  | sub %       | sub %         | 6%                |
+-------------+-------+---------------------+-------------+---------------+-------------------+

nell has quite some influence.

ellmin(sim)   20

Test
====

bg validation
-------------

        * zmax:
                - camb: boltzmann/camb/camb_interface.F90
                - class: class/class_interface.py

        * writing files: cosmosis/datablock/cosmosis_py/block.py:save_to_directory()

